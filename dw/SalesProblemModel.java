/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dw;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

/**
 *
 * @author lct495
 */
public class SalesProblemModel {
    
    private final IloCplex model;
    private final IloNumVar x[];
    private final SalesPlanningProblem problem;

    /**
     * Creates the LP model for the original problem
     * (without decomposition)
     * @param problem
     * @throws IloException 
     */
    public SalesProblemModel(SalesPlanningProblem problem) throws IloException {
        
        this.problem = problem;
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
        
        // 2. Creates the decision variables
        // we have as many decision variables as there are products
        x = new IloNumVar[problem.getnProducts()];
        // Assigns to each position in the array an IloNumVar object 
        // i.e., a decisio variable in Cplex's language
        for(int i = 1; i<= problem.getnProducts(); i++){
            x[i-1] = model.numVar(0, Double.POSITIVE_INFINITY);
        }
        
        
        // 3. Creates the objective function
        // Creates an empty linear numerical expression (linear equation)
        IloLinearNumExpr obj = model.linearNumExpr();
        // Adds terms to the equation
        for(int i = 1; i <= problem.getnProducts(); i++){
            obj.addTerm(-problem.getPrice(i), x[i-1]);
        }
        // Tells cplex to minimize the objective function
        model.addMinimize(obj);
        // Note that the problem is that of maximizing the revenue.
        // This is equivalent to minimizing the negative of the revenue.
        // I transformed the problem as a minimization problem only in order
        // to be consistent with the lectures. 
        
        // 4. Creates the constraints
        // For each constraints creates an populates a linear equation
        
        for(int i = 1; i <= problem.getnProducts(); i++){
            // For each product we need to easure selling a min amount
            IloLinearNumExpr lhs1 = model.linearNumExpr();
            lhs1.addTerm(1, x[i-1]);
            model.addGe(lhs1, problem.getMinSale());
            
            // For each product we need to easure not selling more than 
            // the max allowed
            IloLinearNumExpr lhs2 = model.linearNumExpr();
            lhs2.addTerm(1, x[i-1]);
            model.addLe(lhs2, problem.getMaxSale());
        }
        // We need to ensure not consuming more than the available resources
        IloLinearNumExpr lhs = model.linearNumExpr();
        for(int i = 1; i <= problem.getnProducts(); i++){
            lhs.addTerm(problem.getConsumption(i), x[i-1]);
        }
        model.addLe(lhs, problem.getRawMaterialAvailable());
        
    }
    
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
        System.out.println("Optimal full problem objective value "+model.getObjValue());
        System.out.println("Optimal solution ");
        for(int i = 1; i <= problem.getnProducts(); i++){
            System.out.println("x_"+i+" = "+model.getValue(x[i-1]));
        }
    }
    
    /**
     * Releases all the objects retained by the IloCplex object.
     * In this particular application it makes no difference.
     * However, in bigger and resource-intensive applications
     * it is advised (if not necessary) to release the resources
     * used by the IloCplex object in order for the program to work well.
     * Note that once the method end() has been called, the IloCplex object
     * cannot be used (e.g., queried) anymore.
     */
    public void end(){
        model.end();
    }
    
}
