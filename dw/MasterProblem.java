/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dw;

import ilog.concert.IloColumn;
import ilog.concert.IloException;
import ilog.concert.IloNumVar;
import ilog.concert.IloObjective;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import java.util.HashMap;

/**
 * Describes instances of the master problem. 
 * @author lct495
 */
public class MasterProblem {
    
    private final IloCplex model;
    private final IloObjective objective;
    private final HashMap<Integer,IloNumVar> u;
    private final IloRange convexityConstr;
    private final IloRange complicatingConstr;
    /**
     * Creates a master problem.
     * @param problem
     * @throws IloException 
     */
    public MasterProblem(SalesPlanningProblem problem) throws IloException {
        
        // Creates the necessary IloCplex object
        this.model = new IloCplex();
        
        // Creates the map of decision variables
        // The decision variables of the MP are the convexity coefficients u.
        // We have one variable u for each solution k (column) we provide to MP.
        // However, since we will keep adding columns to MP, we do not know how 
        // many variables we need. That is, the number of variables will
        // increase at each iteration. As a consequence, we cannot
        // use an array of IloNumVar as we do for example in the full problem,
        // In fact, the dimension of arrays is fixed and cannot be expanded.
        // Rather, we use an HashMap (docs here https://docs.oracle.com/javase/8/docs/api/java/util/HashMap.html).
        // An HashMap is, as the name says, a map. We will use it to
        // map the column number (an integer) to is u variable (an IloNumVar).
        // Notice how the map was declared in the class as
        // HashMap<Integer,IloNumVar> u.
        // This means that we associate to each integer value an IloNumVar.
        this.u = new HashMap();
        
        // Creates the constraints
        // The convexity constraint says that the u^k must sum to one
        // Initially, there are no columns, therefore, the lhs is 0
        this.convexityConstr = model.addEq(model.constant(0), 1);
        
        // Then we have the complicating constraint
        // Also in this case the lhs is currently 0.
        this.complicatingConstr = model.addLe(model.constant(0), problem.getRawMaterialAvailable());
        
        // Creates the objective, initially including only a 0 term.
        this.objective = model.addMinimize(model.constant(0));
        
        // As we add columns, the lhs of the constraints and the objective 
        // function will be populated. 
        
    }
    /**
     * Adds a column to MP.
     * @param z the objective coefficient of the column.
     * @param r the coefficient of the complicating constraint.
     * @throws IloException 
     */
    public void addColumn(double z, double r) throws IloException{
        // Given the cost to a new solution (the aggregated cost of 
        // each of the three subproblem solutions) and 
        // the total consumption of the three solutions
        // we build a column and add it to MP.
        
        // Builds a culumn starting from its contribution to the objective
        IloColumn column = model.column(objective, z);
        
        // Extends the column with the contribution to the constraints
        // The new column will have a coefficient of 1 in the 
        // convexity constraint.
        column = column.and(model.column(convexityConstr,1));
        // and will have a coefficient of r in the complicating constraint
        column = column.and(model.column(complicatingConstr,r));
        
        // Finally, we associate a new u variable to this column.
        // The u variable must be between 0 and 1.
        IloNumVar newVariable = model.numVar(column, 0, 1);
        
        // Finally, we store the variable in the map u. 
        // Notice that u.size() returns the number of elements (pairs)
        // currently in the map. Thus, we add variable, say k+1. 
        u.put(u.size()+1, newVariable);
        
    }
    
    /**
     * Solves the subproblem and prints the results
     * @throws IloException 
     */
    public void solve() throws IloException{
        // Setting the output stream to null avoids 
        // that the solver prints information about 
        // the (Dual) Simplex method iterations
        model.setOut(null);
        
        model.solve();
        System.out.println("Optimal solution ");
        for(int i : u.keySet()){
            System.out.println("u_"+i+" = "+model.getValue(u.get(i)));
        }
        System.out.println("Optimal dual solution ");
        System.out.println("Pi "+model.getDual(complicatingConstr));
        System.out.println("Sigma "+model.getDual(convexityConstr));
        
    }
    /**
     * Returns the optimal value of the dual variable Pi
     * @return
     * @throws IloException 
     */
    public double getPi() throws IloException{
        return model.getDual(complicatingConstr);
    }
    /**
     * Returns the optimal value of the dual variable sigma
     * @return
     * @throws IloException 
     */
    public double getSigma() throws IloException{
        return model.getDual(convexityConstr);
    }
    /**
     * Returns the value of the (current) optimal solution.
     * @return the current optimal solution.
     * @throws IloException 
     */
    public double[] getU() throws IloException{
        double U[] = new double[u.size()];
        for(int i : u.keySet()){
            U[i-1] = model.getValue(u.get(i));
        }
        return U;
    }
    /**
     * Returns the (current) optimal objective value.
     * @return the current optimal objective value.
     * @throws IloException 
     */
    public double getObjectiveValue() throws IloException{
        return model.getObjValue();
    }
    
    /**
     * Releases all the objects retained by the IloCplex object.
     * In this particular application it makes no difference.
     * However, in bigger and resource-intensive applications
     * it is advised (if not necessary) to release the resources
     * used by the IloCplex object in order for the program to work well.
     * Note that once the method end() has been called, the IloCplex object
     * cannot be used (e.g., queried) anymore.
     */
    public void end(){
        model.end();
    }
    
    
    
}
