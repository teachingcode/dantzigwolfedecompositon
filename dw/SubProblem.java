/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dw;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

/**
 *
 * @author lct495
 */
public class SubProblem {
    
    private final IloCplex model;
    private final IloNumVar x;

    /**
     * Represents instances of the subproblem.
     * Notice that we have three subproblems, that is three blocks,
     * one for each product. The three subproblems have identical constraints.
     * They change only in the objective coefficient. In addition, at every 
     * iteration we need to set a different objective coefficient representing 
     * the reduced cost. Therefore, we pass the reduced cost as an 
     * argument to the constructor. 
     * @param problem
     * @param reducedCost
     * @throws IloException 
     */
    public SubProblem(SalesPlanningProblem problem, double reducedCost) throws IloException {
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
        
        // 2. Creates the decision variables
        // Each subproblem has only one variable
        x = model.numVar(0, Double.POSITIVE_INFINITY);
        
        // 3. Creates the objective function
        // Creates an empty linear numerical expression (linear equation)
        IloLinearNumExpr obj = model.linearNumExpr();
        // Adds terms to the equation
        obj.addTerm(reducedCost, x);
        // Tells cplex to minimize the objective function
        model.addMinimize(obj);
        
        // 4. Creates the constraints
        // For each constraints creates an populates a linear equation
        IloLinearNumExpr lhs1 = model.linearNumExpr();
        lhs1.addTerm(1, x);
        model.addGe(lhs1, problem.getMinSale());
        
        IloLinearNumExpr lhs2 = model.linearNumExpr();
        lhs2.addTerm(1, x);
        model.addLe(lhs2, problem.getMaxSale());
    
    }
    /**
     * Solves the subproblem and prints the results
     * @throws IloException 
     */
    public void solve() throws IloException{
        // Setting the output stream to null avoids 
        // that the solver prints information about 
        // the (Dual) Simplex method iterations
        model.setOut(null);
        
        model.solve();        
    }
    /**
     * Returns the value of the optimal solution
     * @return
     * @throws IloException 
     */
    public double getSolution() throws IloException{
        return model.getValue(x);
    }
    
    /**
     * Releases all the objects retained by the IloCplex object.
     * In this particular application it makes no difference.
     * However, in bigger and resource-intensive applications
     * it is advised (if not necessary) to release the resources
     * used by the IloCplex object in order for the program to work well.
     * Note that once the method end() has been called, the IloCplex object
     * cannot be used (e.g., queried) anymore.
     */
    public void end(){
        model.end();
    }
    
    
    
}
