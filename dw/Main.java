/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dw;

import ilog.concert.IloException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author lct495
 */
public class Main {

    /**
     * This java program solves a linear program via Dantzig-Wolfe decomposition.
     * The sales planning problem consists of determining the amount
     * of products 1, 2 and 3 to sell. The three products are sold at a price 
     * of 4, 1 and 3 thousand dollars/ton. Due to existing contracts and 
     * regulations, the company must ensure the sale of at least one ton of each 
     * product and no more than two tons of each product. 
     * Finally, the three products require the same raw material. There
     * are 17 packs of material available. The production of the three products
     * requires 3, 2 and 4 packs of raw material per ton of product produced, 
     * respectively. The problem is that of determining the amount of each product
     * to sell in order to maximize the revenue. 
     *
     * The problem is the following:
     * min z = -4x1  -x2   -6x3
     * s.t.     x1                >= 1 
     *          x1                <= 2
     *                x2          >= 1
     *                x2          <= 2
     *                     x3     >= 1
     *                     x3     <= 2
     *          3x1 + 2x2 +4x3    <= 17
     *           x1, x2, x3       >= 0
     * 
     * This problem has clearly a structure with three blocks (n=3) 
     * one complicating constraint (m=1), one variable for each block (n_j=1)
     * and two constraints for each block (m_j=2). The problem is therefore 
     * amenable to DW decomposition. 
     * 
     * We implement the following version of the master problem
     * where columns represent aggregated subproblems solutions. 
     * (Remember that it is possible to write different versions of MP?
     * have a look at section "Variations of the Decomposition Process" 
     * in the Dantzig and Wolfe article). 
     * 
     * min sum_k z_k u_k
     * s.t.      sum_k r_k u_k <= 17
     *           sum_k u_k = 1;
     *           u_k >= 0
     * where k represent columns considered in MP. The number of columns
     * increases at each iteration. z_k = -4 x1^k - x2^k - 6 x3^k 
     * is the aggregated price of the three subproblem solutions, 
     * while r_k = 3 x1^k + 2 x2^k + 4 x3^k is the total consumption
     * of the three product in the k-th solution to the subproblems, 
     * where x1^k, x2^k and x3^k are obtained by solving the subproblems. 
     * 
     * @param args the command line arguments
     * @throws ilog.concert.IloException
     */
    public static void main(String[] args) throws IloException {
        
        
        
        // ================================================================
        // DATA
        // ================================================================
        // Data. Here we create some data that will be useful later on
        int nProducts = 3;
        // The original objective coefficients
        double prices[] = {4, 1 , 6};
        // The coefficients of the variables in the complicating constraint
        double consumption[] = {3, 2, 4};
        
        // ================================================================
        // CREATES AN INSTANCE OF THE SALES PROBLEM
        // ================================================================
        SalesPlanningProblem problem = new SalesPlanningProblem(nProducts,1,2,17,consumption,prices);
        
        // ================================================================
        // CREATES THE MASTER PROBLEM
        // ================================================================
        // Creates the Master Problem which initially
        // does not contain any columns (see the constructor for details).
        MasterProblem mp = new MasterProblem(problem);
        
        // ================================================================
        // INITIALIZATION
        // ================================================================
        // Initially, MP is empty: it does not contain any column.
        // In order to provide a number of initial columns, we solve 
        // each subproblem a number times, every time with an 
        // arbitrarily different objective function. The scope is that of 
        // obtaining an initial set of corner points of the feasible
        // region of the subproblems which MP can use to get started.
        // We arbitrarily choose to start with two solutions for 
        // each subproblem.
        int nColumns = 3;
        
        // Each column is obtained by the solutions to the subproblems. 
        // A solution if given by the x solution to the three subproblems,
        // that is, the amount sold by the three subproblems. 
        // In order to trace back the solution to the original
        // problem from a solution to MP, we need to store the solutions we 
        // obtain to the subproblems. To this scope, we create a map which
        // stores, for all columns, the subproblem solutions which generated
        // the column. The column is indicated by an integer and the solution
        // is an array of 3 double representing x1,x2,x3.
        HashMap<Integer,double[]> solutions = new HashMap();
        // Read more about HashMaps here 
        // https://docs.oracle.com/javase/8/docs/api/java/util/HashMap.html
        
        // In addition, we create a list containing the solution(s) to 
        // add to MP in the next iteration. This is different from
        // solutions: solutions memorizes all solutions, while newSolutions
        // memorizes only the solution(s) which MP needs to consider in 
        // the next iteration. As we will see later, newSolution will be
        // emptied every time the solution it contains have been added.
        ArrayList<double[]> newSolutions = new ArrayList();
        // Read more about ArrayLists here
        // https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html
        
        // Note A. In this particular application,
        // the three subproblems are identical in the constraints
        // and we could achieve the same scope using only one SubProblem
        // object (it would be more memory efficient) and changing its objective
        // function, rather than building from scratch a new subproblem every time. 
        // However, in general, subproblems are different also in the constraints. 
        // Therefore, it is good practice to start seeing them as separated entities. 
        // Note B. We will create a new set of subproblems every
        // time we need to modify them (e.g., at each iteration).
        // We could achieve the same results (possibly more efficiently) 
        // by creating the three subproblems only at the beginning and modifying 
        // their objective functions when needed. However, for now we use 
        // this simplistic approach to get started. 
        // We learn how to stroll before we can run. 
        
        // Here we obtain the first nColumns for provide to MP.
        // We do that in a loop. 
        System.out.println("// =================================");
        System.out.println("0. INITIALIZATION");
        System.out.println("// =================================");
        System.out.println("// Obtaining the first "+nColumns+" columns.");
        
        for(int i = 1; i <= nColumns; i++){
            // We arbitrary set the the cost vector
            // by adding 2 * i to the original prices. 
            double arbitraryPrices[] = new double[problem.getnProducts()];
            for(int j = 1; j <= problem.getnProducts(); j++){
                arbitraryPrices[j-1] = problem.getPrice(j) + 2*i;
            }
            
            // We create a new container for the solution obtained.
            // Note that, for each subproblem, the solution is a single value
            // that is, the value of x_i for subproblem i=1,...,3
            double currentSolution[] = new double[problem.getnProducts()];
            // This solution will then be stored in the map named "solutions".
            
            // Now we solve the subproblems. 
            for(int j = 1; j <= problem.getnProducts(); j++){
                // Creates the subproblem passing the arbitrary price
                SubProblem subproblem = new SubProblem(problem,arbitraryPrices[j-1]);
                // Solves the subproblem
                subproblem.solve();
                // Obtains and stores the solution x_j
                currentSolution[j-1] = subproblem.getSolution();
                System.out.println("X1 = "+currentSolution[j-1]);
                // Releases the resources
                subproblem.end();
            }
            // Stores the solutions in the map
            solutions.put(i,currentSolution);
        }
                
        
        // At the initialization phase the solution to add
        // are the nColumns arbitrary solutions we generated.
        for(int i : solutions.keySet()){
            newSolutions.add(solutions.get(i));
        }
        
        
        // =========================================================
        // ITERATIONS
        // =========================================================
        // Now we start iterating between MP and the subproblems
        // untill the method has converged
        boolean converged = false;
        int iteration = 0;
        
        while(!converged){
            System.out.println("// =================================");
            System.out.println("// ITERATION "+iteration);
            System.out.println("// =================================");
            
            // First we add MP one column for each new solution
            // found in the previous iteration. 
            // A column is made of the aggregated cost and 
            // aggregated consumption of the subproblem solutions. 
               
            System.out.println("Passing the following solutions to MP");
            for(int j = 1; j <= newSolutions.size(); j++){ 
                // The cost z of each solution is made of the sum of 
                // the cost of the variable of each block. 
                // The same applies for the lhs r.
                double z = 0;
                double r = 0;
                for(int i = 1; i <= problem.getnProducts(); i++){
                    double[] x = newSolutions.get(j-1);
                    z+= -problem.getPrice(i) * x[i-1];
                    r+= problem.getConsumption(i) * x[i-1];
                }
                
                // We print the resulting column
                System.out.println("z("+j+") = "+z);
                System.out.println("r("+j+") = "+r);
                
                // We add the column to MP. 
                mp.addColumn(z, r);
            }
            
            // We empty the new solutions list, meaning that 
            // we have added the ones we needed to add and now we 
            // look for new ones.
            newSolutions.clear();
            
            // MASTER PROBLEM SOLUTION
            // We solve MP with the new columns and get
            // the necessary solution information
            
            System.out.println("// =================================");
            System.out.println("// MASTER PROBLEM SOLUTION ITERATION "+iteration);
            System.out.println("// ================================="); 

            mp.solve();
            
            // We save pi, sigma and u 
            double pi = mp.getPi();
            double sigma = mp.getSigma();
            double[] u = mp.getU();
            
            // In addition, the optimal objective value to MP at each iteration
            // provides an upper bound on the optimal objective value
            System.out.println("Upper bound : "+mp.getObjectiveValue());
            
            
            // SUBPROBLEMS SOLUTION
            // Now we solve the subproblems with changed objective 
            // functions. The objective functions now represent the reduced costs.
            // Remember the reduced cost (c_j - piA_j)x_j - sigma
            // and the scope of the subproblem is that of finding new 
            // columns for MP with negative reduced costs (if they exist).
        
            // First we calculate the variable part of the reduced costs,
            // that is the part which multiplies x, c_j - pi A_j
            System.out.println("Calculated the following new costs for the subproblems:");
            double variableReducedCosts[] = new double[problem.getnProducts()];
            for(int i = 1; i <= problem.getnProducts(); i++){
                // Remember the reduced cost for subproblem i 
                // c_i - pi * A_i 
                // In this case c_i is just the price (well, -1 * price since we are minimizing instead 
                // of maximizing), and A_i is the (matrix of) coefficient(s) of 
                // block i in the complicating constraint. In this case A_i is
                // simply the coefficient of the variable x_i in the complicating constraint. 
                variableReducedCosts[i-1] = -problem.getPrice(i) - (pi * problem.getConsumption(i));
                
                // We round sufficiently small values.
                // Sometimes extremely low value of the reduced
                // costs may lead to incorrect calculations or
                // to considering optimality conditions not verified when they are actually verified. 
                if(Math.abs(variableReducedCosts[i-1]) <= 1e-06){
                    variableReducedCosts[i-1] = 0;  
                }
                
                System.out.println("Reduced Cost "+i + " = "+variableReducedCosts[i-1]);
            }
            
            System.out.println("// =================================");
            System.out.println("// SUBPROBLEMS SOLUTION ITERATION "+iteration);
            System.out.println("// =================================");
            
            // We create a container for the solutions of the subproblems 
            double subProblemSolution[] = new double[problem.getnProducts()];
            for(int i = 1; i <= problem.getnProducts(); i++){
                // Creates the subproblem
                SubProblem subproblem = new SubProblem(problem,variableReducedCosts[i-1]);
                // Solves the subproblem
                subproblem.solve();
                // Obtains the solution x
                subProblemSolution[i-1] = subproblem.getSolution();
                System.out.println("X"+i+" = "+subProblemSolution[i-1]);
                // Releases the resources
                subproblem.end();
            }
            
            // We calculate the variable part of the reduced cost
            // for the new solution obtained. In this case
            // since we add one aggregated column, we 
            // calculate delta as sum_j (c_j - pi A_j)x_j - sigma
            double delta = 0;
            for(int i = 1; i <= problem.getnProducts(); i++){
                delta+= variableReducedCosts[i-1] * subProblemSolution[i-1];
            }
            delta = delta - sigma;
            
            // From the solutions to the subproblems we can obtain 
            // a lower bound on the optimal objective value
            // LB = v + Pi * b
            // Where v = sum_j ( c_j - Pi Aj x_j)
            // That this the sum of the objective values to the subproblems
            // which corresponds to (delta + sigma) 
            System.out.println("Lower Bound = "+( (delta+sigma) + pi * 17));
            
            // CONVERGENCE
            // Did we find a solution with negative reduced cost?
            System.out.println("// =================================");
            System.out.println("// CONVERGENCE CHEKING "+iteration);
            System.out.println("// =================================");
            converged = (delta >= 0);
            if(converged){
                System.out.println("Convergence test:  "+converged);    
                System.out.println("delta ="+delta); 
                System.out.println("We found no more solutions with negative reduced cost.");
                System.out.println("// ====================================");
                System.out.println("The optimal DW solution is:");
                System.out.println("// ====================================");
                double x[] = new double[problem.getnProducts()];
                for(int j = 1; j <= problem.getnProducts(); j++){
                    for(int i = 1; i <= nColumns; i++){
                        x[j-1]+= solutions.get(i)[j-1] * u[i-1];
                    }
                    System.out.println("X"+j+" = "+x[j-1]);
                }
                System.out.println("Optimal objective value ");
                double optimal = 0;
                for(int i = 1;i <= problem.getnProducts(); i++){
                    optimal+= x[i-1] * (-problem.getPrice(i));
                }
                System.out.println(optimal);
                    
            }else{
                System.out.println("Convergence test:  "+converged);    
                System.out.println("delta = "+delta); 
                System.out.println("We found one more solution with negative reduced cost.");
                // We add the solution to the subproblems to the list
                // of solutions for next iteration
                nColumns++;
                solutions.put(nColumns, subProblemSolution);
                newSolutions.add(subProblemSolution);
            }
            
            iteration++;
        }
        System.out.println("// =================================== ");
        
        // Checks if the value coincides with the objective of the full problem
        // Creates and solves the full problem
        SalesProblemModel model = new SalesProblemModel(problem);
        model.solve();
        
        
    }
    
}
