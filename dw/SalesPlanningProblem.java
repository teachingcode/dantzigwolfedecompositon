/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dw;

/**
 * This class represents the template of the Sales Planning Problem.
 * @author lct495
 */
public class SalesPlanningProblem {
    
    private final int nProducts;
    private final double minSale;
    private final double maxSale;
    private final double rawMaterialAvailable;
    private final double consumption[];
    private final double price[];

    public SalesPlanningProblem(int nProducts, double minSale, double maxSale, double rawMaterialAvailable, double[] consumption, double[] price) {
        this.nProducts = nProducts;
        this.minSale = minSale;
        this.maxSale = maxSale;
        this.rawMaterialAvailable = rawMaterialAvailable;
        this.consumption = consumption;
        this.price = price;
    }

    public int getnProducts() {
        return nProducts;
    }

    public double getMinSale() {
        return minSale;
    }

    public double getMaxSale() {
        return maxSale;
    }

    public double getRawMaterialAvailable() {
        return rawMaterialAvailable;
    }

    public double[] getConsumption() {
        return consumption;
    }

    public double[] getPrice() {
        return price;
    }
    
    public double getPrice(int product){
        return price[product-1];
    }
    public double getConsumption(int product){
        return consumption[product-1];
    }
    
    
    
}
